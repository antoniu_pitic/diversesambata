#include <iostream>
using namespace std;
#include "CifreNumar.h"

void AfiseazaAleaCareIncepCuCifraImparaDinIntervalul(int a, int b)
{
	for (int i = a; i <= b; i++) {
		if (IncepeCuCifraImpara(i)) {
			cout << i << " ";
		}
	}
}

void AfiseazaAleaCareIncepCuCifraParaDinIntervalul(int a, int b)
{
	for (int i = a; i <= b; i++) {
		if (!IncepeCuCifraImpara(i)) {
			cout << i << " ";
		}
	}

}

bool IncepeCuCifraImpara(int n)
{
	int primaCifra = PrimaCifra(n);
	return primaCifra % 2 == 1;

	/*
	
	if (primaCifra%2==1){
		return true;
	{
	else{
		return false;
	}
	
	*/
}

int PrimaCifra(int n)
{
	while (n > 9) {
		n = n / 10;
	}
	return n;
}

void AfiseazaAleaPalindroame(int a, int b)
{
	for (int i = a; i <= b; i++) {
		if (Palindrom(i)) {
			cout << i << " ";
		}
	}
}

int CatePalindroame(int a, int b)
{
	int ct = 0;
	for (int i = a; i <= b; i++) {
		if (Palindrom(i)) {
			ct++;
		}
	}
	return ct;
}

bool Palindrom(int n)
{
	return n==Invers(n);
}

int Invers(int n)
{
	int m = 0;
	for (int t = n; t; t /= 10) {
		m = m * 10 + t % 10;
	}
	return m;
}
